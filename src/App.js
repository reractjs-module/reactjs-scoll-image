
import './App.css';

import ChangeNameComponen from './componen/ChangeNameComponen';
import FlexLayoutComponen from './componen/FlexLayoutComponen';

function App() {
 
  return (
    <div className="Home">
      <ChangeNameComponen></ChangeNameComponen>
      <FlexLayoutComponen></FlexLayoutComponen>
    </div>
  );
}


export default App;
