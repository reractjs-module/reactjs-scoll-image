

import { useState } from 'react';

function ChangeNameComponen() {
  const [nama, setNama] = useState('World!');

  const changeName = () =>{
    setNama("Fiqri")

  }
  return (
    <div>
      <h1>Hello {nama}</h1>
      <p>Welcome, Fiqri</p>
      <button onClick={changeName}>Click Here</button>
    </div>
  );
}

export default ChangeNameComponen;
