import spiderman1 from "../asset/spiderman02.jpg"
import spiderman2 from "../asset/spiderman3.jpg"
import spiderman3 from "../asset/spiderman4.jpg";
import "./FlexLayoutComponen.css";  
import SuperheroCard from "./SuperheroCard";


const SUPERHERO_LIST = [
    {
        nama:"Siperman1",
        imageUrl: spiderman1
    },
    {
        nama :"Spiderman2",
        imageUrl: spiderman2 
    },
    {
        nama :"Spiderman3",
        imageUrl: spiderman3
    }
    
];

const string = ['']

function FlexLayoutComponen(){
return (
    <div className="FlexContainer">
    <h1>SuperHeroList</h1>
    <div className="CardList">
    {SUPERHERO_LIST.map((item, key) => (
        <SuperheroCard key={key} name={item.nama} url ={item.imageUrl}></SuperheroCard>
    ))}
    </div>
    </div>
    );
}

export default FlexLayoutComponen;