function SuperheroCard(props){
  return(
    <div className="card">
      <img src={props.url}></img>
      <label>{props.name}</label>
    </div>
  )
}

export default SuperheroCard;